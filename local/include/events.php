<?php

use \Bitrix\Main\EventManager;
use \Bitrix\Main\Mail\Event;

$eventManager = EventManager::getInstance();

/**
 * Добавление пользователя при регистрации в группу "Физические лица",
 * либо "Партнеры".
 */
$eventManager->addEventHandler('main', 'OnBeforeUserAdd', 'OnBeforeUserAddHandler');
function OnBeforeUserAddHandler(&$arFields)
{
    //Получить id групп пользователей
    $groupsData = [];
    $rsGroups = CGroup::GetList($by = "c_sort", $order = "asc", ["STRING_ID" => "INDIVIDUALS|PARTNERS"]);
    while ($arGroups = $rsGroups->Fetch()) {
        $groupsData[$arGroups['STRING_ID']] = $arGroups['ID'];
    }

    if ($arFields['UF_NAME_LEGAL_ENTITY'] == 'NONE'
        && $arFields['UF_INN'] == 'NONE'
        && $arFields['UF_PARTNER_ID'] == 'NONE') {
        $arFields['GROUP_ID'][] = $groupsData['INDIVIDUALS'];
    } else {
        $arFields['GROUP_ID'][] = $groupsData['PARTNERS'];
    }
}

$eventManager->addEventHandler('main', 'OnAfterUserAdd', 'OnAfterUserAddHandler');
function OnAfterUserAddHandler(&$arFields)
{
    //Отправка почтового уведомления после регистрации партнера
    if (in_array(11, $arFields['GROUP_ID'])) {
        $arFields = [
            'NAME' => $arFields['NAME'],
            'LAST_NAME' => $arFields['LAST_NAME'],
            'EMAIL' => $arFields['EMAIL'],
            'LOGIN' => $arFields['LOGIN'],
            'NAME_LEGAL_ENTITY' => $arFields['UF_NAME_LEGAL_ENTITY'],
            'INN' => $arFields['UF_INN'],
            'PARTNER_ID' => $arFields['UF_PARTNER_ID']
        ];
        Event::send([
            'EVENT_NAME' => 'NEW_PARTNER',
            'LID' => 's1',
            'C_FIELDS' => $arFields
        ]);
    }
}