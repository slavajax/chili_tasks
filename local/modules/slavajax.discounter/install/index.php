<?php
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class slavajax_discounter extends CModule
{
    public $MODULE_ID = 'slavajax.discounter';
    public $MODULE_VERSION;
    public $MODULE_VERSION_DATE;
    public $MODULE_NAME;
    public $MODULE_DESCRIPTION;
    public $MODULE_GROUP_RIGHTS = 'Y';

    public function __construct()
    {
        $arModuleVersion = array();
        include __DIR__.'/version.php';

        $this->MODULE_VERSION = $arModuleVersion['VERSION'];
        $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];

        $this->PARTNER_NAME = 'Slavajax';
        $this->PARTNER_URI = 'https://vk.com/slavajax';

        $this->MODULE_NAME = Loc::getMessage('SLAVAJAX_DISCOUNTER_MODULE_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('SLAVAJAX_DISCOUNTER_MODULE_DESCRIPTION');
    }

    public function InstallEvents()
    {
        return true;
    }

    public function UnInstallEvents()
    {
        return true;
    }

    public function InstallDB()
    {

        return true;
    }

    public function UnInstallDB()
    {

        return true;
    }

    public function InstallFiles()
    {

        return true;
    }

    public function UnInstallFiles()
    {

        return true;
    }

    public function DoInstall()
    {

        $this->InstallFiles();

        RegisterModule($this->MODULE_ID);
    }

    public function DoUninstall()
    {

        $this->UnInstallFiles();

        UnRegisterModule($this->MODULE_ID);
    }
}