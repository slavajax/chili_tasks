<?php

namespace slavajax\Discounter;

use \Bitrix\Main\GroupTable;

class ToolsDiscount
{
    protected static $_instance = [];

    public static function getInstance()
    {
        $calledClass = get_called_class();
        if (!isset(self::$_instance[$calledClass])) {
            self::$_instance[$calledClass] = new self();
        }
        return self::$_instance[$calledClass];
    }

    /**
     * Получить id группы "Партнеры"
     * @return mixed
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function getPartnersGroupId()
    {
        return GroupTable::getList([
            'select' => ['ID', 'NAME', 'STRING_ID'],
            'filter' => ['STRING_ID' => ['PARTNERS']]
        ])->fetch()['ID'];
    }
}