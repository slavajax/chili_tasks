<?php

namespace slavajax\Discounter;

use \Bitrix\Main\Loader;

Loader::registerAutoLoadClasses(
    'slavajax.discounter',
    [
        SaleDiscount::class => 'lib/SaleDiscount.php',
        CouponDiscount::class => 'lib/CouponDiscount.php',
        UserDiscount::class => 'lib/UserDiscount.php',
        ToolsDiscount::class => 'lib/ToolsDiscount.php'
    ]
);