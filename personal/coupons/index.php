<?
define("HIDE_SIDEBAR", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Купоны");

$currentUserId = $USER->GetID();
$arCoupon = \Bitrix\Sale\Internals\DiscountCouponTable::getRow([
    'filter' => ['USER_ID' => $currentUserId, 'ACTIVE' => 'Y']
]);
$coupon = ($arCoupon['COUPON']) ?? '';
?>
<?if ($coupon):?>
    <h3>Ваш промокод: <i><?=$coupon?></i></h3>
<?endif;?>

<br><br><p>Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов.</p>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>