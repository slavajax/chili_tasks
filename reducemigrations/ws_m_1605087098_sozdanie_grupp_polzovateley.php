<?php

/**
 * Class definition update migrations scenario actions
 **/
class ws_m_1605087098_sozdanie_grupp_polzovateley extends \WS\ReduceMigrations\Scenario\ScriptScenario {

    /**
     * Name of scenario
     **/
    static public function name() {
        return "Создание групп пользователей";
    }

    /**
     * Priority of scenario
     **/
    static public function priority() {
        return self::PRIORITY_HIGH;
    }

    /**
     * @return string hash
     */
    static public function hash() {
        return "97c4ac239390a74adaacf41ae889fdc08403eff3";
    }

    /**
     * @return int approximately time in seconds
     */
    static public function approximatelyTime() {
        return 2;
    }

    /**
     * Write action by apply scenario. Use method `setData` for save need rollback data
     **/
    public function commit() {
        //Создание группы "Физические лица"
        $group = new CGroup();
        $arFields = [
            'ACTIVE' => 'Y',
            'C_SORT' => 400,
            'NAME'  => 'Физические лица',
            'DESCRIPTION' => 'Группа для всех пользователей',
            "STRING_ID"      => "INDIVIDUALS"
        ];
        $group->Add($arFields);

        //Создание группы "Партнеры"
        $arFields = [
            'ACTIVE' => 'Y',
            'C_SORT' => 500,
            'NAME'  => 'Партнеры',
            'DESCRIPTION' => 'Группа для партнеров',
            "STRING_ID"      => "PARTNERS"
        ];
        $group->Add($arFields);
    }

    /**
     * Write action by rollback scenario. Use method `getData` for getting commit saved data
     **/
    public function rollback() {
        //Получить id групп для удаления
        $groupsToDelete = [];
        $rsGroups = CGroup::GetList($by = "c_sort", $order = "asc", ["STRING_ID" => "INDIVIDUALS|PARTNERS"]);
        while($arGroups = $rsGroups->Fetch())
        {
            $groupsToDelete[$arGroups['STRING_ID']] = $arGroups['ID'];
        }
        //Удаление группы "Физические лица"
        CGroup::Delete($groupsToDelete['INDIVIDUALS']);

        //Удаление группы "Партнеры"
        CGroup::Delete($groupsToDelete['PARTNERS']);
    }
}